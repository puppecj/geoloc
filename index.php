<?php
/**
 * Simply a placeholder file to prevent directory listing..
 *
 * @package growth-fundamentals
 */

header( 'HTTP/1.0 404 Not Found' );
die;
